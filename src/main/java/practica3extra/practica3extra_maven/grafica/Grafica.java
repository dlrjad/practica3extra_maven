package practica3extra.practica3extra_maven.grafica;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.swing.JOptionPane;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;

import practica3extra.practica3extra_maven.fichero.Fichero;

public class Grafica {
	
	String name;
	int nominations;
	HashMap<String, Grafica> datosGrafica = new HashMap<String, Grafica>();
	HashMap<String, String> datosFichero = new HashMap<String, String>();
	
	public Grafica(String path) {

		Fichero fich = new Fichero();
		HashMap<String, String> datosFichero = fich.leerFicheroDatos(path);
		
		this.datosFichero = datosFichero;
		
		Iterator<?> it = datosFichero.keySet().iterator();
		while(it.hasNext()){
			String key = (String) it.next();
			int value = Integer.parseInt((String) datosFichero.get(key));

			datosGrafica.put(key, new Grafica(key, value));
			
		}
		
	}
	
	public Grafica(String name, int nominations) {
		
		this.name = name;
		this.nominations = nominations;
		
	}
	
	public String getName() {
		
		return this.name;
		
	}
	
	public int getNominatios() {
		
		return this.nominations;
		
	}
	
	public void mostrarGrafica(String title, JFreeChart chart) {
		
		ChartFrame frame = new ChartFrame(title, chart);
        frame.pack();
        frame.setVisible(true);
		
	}
	
	public List<Grafica> ordenarDatos() {
		
		List<Grafica> listDatos = new ArrayList<Grafica>(datosGrafica.values());
		
		/*Comparator<String> comparador = Collections.reverseOrder();
		Collections.sort(listaValores, comparador);*/
		
		Collections.sort(listDatos, new Comparator<Grafica>() {

			public int compare(Grafica datos1, Grafica datos2) {
				// TODO Auto-generated method stub
				return datos2.getNominatios()-datos1.getNominatios();
			}

	    });
		
		return listDatos;
				
	}
	
	public void graficaCircular() {
		
		DefaultPieDataset pieDataset = new DefaultPieDataset();
		
		Iterator<?> it = datosFichero.keySet().iterator();
		while(it.hasNext()){
			String key = (String) it.next();
			int value = Integer.parseInt((String) datosFichero.get(key));
			//System.out.println("Datos de " + key + " nominado " + datosFichero.get(key));
			pieDataset.setValue("Datos de " + key + " nominado " + datosFichero.get(key) , new Integer(value));
			
		}

        JFreeChart chart = ChartFactory.createPieChart(
        		"Grafica circular",
                pieDataset,
                true,
                true,
                false
        );

        mostrarGrafica("Grafica Circular", chart);

	}
	
	public void graficaCircularMasNominados(int numNominados) {
		
		List<Grafica> listDatos = ordenarDatos();

		DefaultPieDataset pieDataset = new DefaultPieDataset();
		
		if(numNominados<=listDatos.size()) {
		for (int index=0; index<numNominados; index++) {
			pieDataset.setValue("Datos de " + listDatos.get(index).getName() + " nominado " + listDatos.get(index).getNominatios() , new Integer(listDatos.get(index).getNominatios()));
	    }
		
			JFreeChart chart = ChartFactory.createPieChart(
					"Grafica los "+ numNominados +" mas nominados",
					pieDataset,
					true,
		            true,
		            false
		    );
	
			mostrarGrafica("Grafica "+ numNominados +" mas nominados", chart);
		
		}else {
			JOptionPane.showMessageDialog(null, "Superas limite para grafica más nominados ");
		}
		
	}

}
