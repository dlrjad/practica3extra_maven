package practica3extra.practica3extra_maven.fichero;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;


public class Fichero {

	public HashMap<String, String> leerFicheroDatos(String path) {
		
		String linea;		
		HashMap<String, String> datosFichero = new HashMap<String, String>();
 
        BufferedReader in;
		try {
			
			in = new BufferedReader (new InputStreamReader (new FileInputStream(path), "UTF-8"));
			
			while ((linea = in.readLine())!=null) {
				String[] arrayDeCadenas = linea.split(";");

				if(datosFichero.get(arrayDeCadenas[0]) == null) {
					datosFichero.put(arrayDeCadenas[0], "1");
				}else {
					int valor = Integer.parseInt(datosFichero.get(arrayDeCadenas[0]));
					datosFichero.put(arrayDeCadenas[0], String.valueOf(++valor));
				}
			}
			
				
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return datosFichero;
        
	}
	
}
